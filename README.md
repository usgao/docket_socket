# docket socket

This is the folder that contains the code necessary to run Docket Socket

Many of the files were initially created by Django automatically.  

The command `django-admin startproject mysite` will create the initial files where `mysite` is the name of the project you would like to create. In this case it was `docket`. This creates the overall layout of the site.

 This created the following structure:

    docket/
         manage.py
         docket/
             __init__.py
             settings.py
             urls.py
             wsgi.py"""


Then the command `python manage.py startapp downloader` was run which creates the application `downloader` on the overall website. This command also initializes a series of files

This created the following structure within the docket directory:

    downloader/
           __init.__py
           admin.py
           apps.py
           migrations/
               __init__.py
               models.py
               test.py
               views.py"""

Note some of these initialized files were later deleted because they are not used. Models.py for example is a file which can create data structures that populate the database, but were not necessary for this project so were removed. The Migrations folder also likely could be removed entirely since that is to record changes in the models

files that were added manually include the templates directory in the download folder. This directory contain the html files that are loaded.  

The primary file that was modified was the views.py file that contains the majority of the custom code for this application to run.

## Other Notes:
- settings.py is an important file that allows you to adjust things like whether debugger is on. What hosts are allowed to connect what database to use if you are using a database (sqlite3 is the default). It is also necessary to list all of the apps you have created in the settings file. In our case, we have added 'downloader' to the list of installed apps
- urls.py is two different files in different directories. The one in the outer directory controls the whole website. The other is specific to the downloader app (in this case that's the only app)
- apps.py tells Django what apps are currently installed (in this case just downloader)
- the html files in the html directory are mostly plain html but contain a few specific Django calls such as the djagno form 
